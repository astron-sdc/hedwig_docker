FROM python:3.11.8

WORKDIR /app

RUN git clone https://github.com/eaobservatory/hedwig.git

WORKDIR /app/hedwig

COPY hedwig.ini etc/hedwig.ini

RUN apt-get update && apt-get install -y libmagic-dev

RUN python -m venv /opt/venv

COPY requirements.txt requirements.txt

RUN /opt/venv/bin/pip install -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/app/hedwig/lib"

COPY start.sh .

RUN chmod +x start.sh

CMD ["./start.sh"]