Setup for running a containerized **_LOCAL DEVELOPMENT TEST INSTANCE WITH HARD CODED CREDENTIALS_** of [Hedwig](https://github.com/eaobservatory/hedwig). Running the `docker-compose.yml` file will result in two running containers:

* A postgres database on port 5432
* A flask web app on port 5678



## Instructions

* Execute `./run_hedwig.sh`
* Browse to http://localhost:5678

